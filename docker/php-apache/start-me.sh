#!/bin/bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

# if [ "$env" != "local" ]; then
#     echo "Caching configuration..."
#     php artisan optimize
# fi

php -d memory_limit=-1 /usr/local/bin/composer install
php artisan optimize:clear
php artisan migrate --force
php artisan passport:install
php artisan db:seed

if [ "$role" = "app" ]; then
    exec apache2-foreground
else
    echo "Could not match the container role \"$role\""
    exit 1
fi
