<?php

namespace App\Http\Controllers;

use App\Models\BookReader;
use App\Models\Librarian;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * --------------------------------------------
     * Once a month, identify a librarian with the
     * largest number of books issued
     * (even if they have not been returned)
     * --------------------------------------------
     *
     * @return JsonResponse
     */
    public function index () : JsonResponse
    {
        $librarianId = BookReader::select('librarian_id')->groupBy('librarian_id')
            ->orderBy(DB::raw("COUNT(0)"), 'DESC')->first()->librarian_id;
        $librarian = Librarian::select('id', 'name')->where('id', $librarianId)->first();
        return response()->json([
            'status' => 'success',
            'librarian' => $librarian
        ]);
    }
}
