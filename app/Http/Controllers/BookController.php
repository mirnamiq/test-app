<?php

namespace App\Http\Controllers;

use App\Http\Requests\GiveBookBackRequest;
use App\Http\Requests\IssueBookRequest;
use App\Models\Book;
use App\Models\BookReader;
use App\Models\Reader;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class BookController extends Controller
{
    /**
     * ----------------------------
     * Retrieve all books
     * ----------------------------
     *
     *
     * @return JsonResponse
     */
    public function index (): JsonResponse {
        return response()->json([
            'status' => 'success',
            'books' => Book::select('id', 'name')->get()
        ]);
    }


    /**
     * ----------------------------
     * Issue book
     * ----------------------------
     *
     * @param IssueBookRequest $request
     * @return JsonResponse
     */
    public function issueBook (IssueBookRequest $request): JsonResponse
    {
        $readerId = Reader::where('code', $request->reader_code)->first()->id;
        if(BookReader::where('reader_id', $readerId)->whereNull('give_back_datetime')->count())
        {
            return response()->json([
               'status' => 'error',
               'message' => 'There is a book that this reader did not give back'
            ]);
        }
        $bookReader = new BookReader();
        $bookReader->book_id = $request->book_id;
        $bookReader->librarian_id = $request->librarian_id;
        $bookReader->reader_id = $readerId;
        $bookReader->interval_from = $request->from;
        $bookReader->interval_to = $request->to;
        $bookReader->save();
        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * ----------------------------
     * Give Book Back
     * ----------------------------
     *
     * @param GiveBookBackRequest $request
     * @return JsonResponse
     */
    public function giveBookBack (GiveBookBackRequest $request) : JsonResponse
    {
        $readerId = Reader::where('code', $request->reader_code)->first()->id;
        $bookReader = BookReader::where('reader_id', $readerId)->where('give_back_datetime')->first();
        if(!$bookReader)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'There is not any book that this reader should give back'
            ]);
        }
        $bookReader->give_back_datetime = Carbon::now();
        $bookReader->save();
        return response()->json([
            'status' => 'success'
        ]);
    }
}
