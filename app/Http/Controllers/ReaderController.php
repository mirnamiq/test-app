<?php

namespace App\Http\Controllers;

use App\Models\Reader;
use Illuminate\Http\JsonResponse;

class ReaderController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index (): JsonResponse {
        return response()->json([
            'status' => 'success',
            'readers' => Reader::select('name', 'code')->get()
        ]);
    }
}
