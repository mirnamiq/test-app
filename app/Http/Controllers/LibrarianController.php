<?php

namespace App\Http\Controllers;

use App\Models\Librarian;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class LibrarianController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index (): JsonResponse {
        return response()->json([
            'status' => 'success',
            'librarians' => Librarian::select('id', 'name')
                ->where('work_days_is_even', !(Carbon::today()->day % 2))->get()
        ]);
    }
}
