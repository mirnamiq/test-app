<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    public function authors():object
    {
        return $this->hasManyThrough(Author::class, BookAuthor::class);
    }
}
