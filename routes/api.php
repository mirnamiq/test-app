<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::get('librarians', [\App\Http\Controllers\LibrarianController::class, 'index']);
Route::get('readers', [\App\Http\Controllers\ReaderController::class, 'index']);
Route::get('books', [\App\Http\Controllers\BookController::class, 'index']);
Route::post('issue-book', [\App\Http\Controllers\BookController::class, 'issueBook']);
Route::post('give-book-back', [\App\Http\Controllers\BookController::class, 'giveBookBack']);
Route::get('report', [\App\Http\Controllers\ReportController::class, 'index']);
