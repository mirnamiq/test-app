<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookReadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_readers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('book_id');
            $table->foreignId('librarian_id');
            $table->foreignId('reader_id');
            $table->date('interval_from');
            $table->date('interval_to');
            $table->dateTime('give_back_datetime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_readers');
    }
}
