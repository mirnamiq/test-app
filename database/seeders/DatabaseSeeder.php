<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\BookAuthor;
use App\Models\BookReader;
use App\Models\Librarian;
use App\Models\Reader;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Librarian::factory(10)->create();
        Reader::factory(10)->create();
        Author::factory(10)->create();
        Book::factory(50)->create();
        BookAuthor::factory(300)->create();
        BookReader::factory(10)->create();
    }
}
