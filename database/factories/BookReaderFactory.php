<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\Librarian;
use App\Models\Reader;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class BookReaderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $start = $this->faker->date();
        return [
            'book_id' => Book::factory(),
            'reader_id' => Reader::whereNOTIn('id', [DB::raw("(SELECT reader_id FROM book_readers WHERE give_back_datetime IS NULL)")])
                ->inRandomOrder()->first()->id,
            'librarian_id' => Librarian::factory(),
            'interval_from' => $start,
            'interval_to' => (new Carbon($start))->addDays(rand(1, 30)),
            'give_back_datetime' => $this->faker->boolean ? Carbon::now() : null
        ];
    }
}
