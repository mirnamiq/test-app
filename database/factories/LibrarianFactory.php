<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class LibrarianFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'work_days_is_even' => $this->faker->boolean()
        ];
    }
}
